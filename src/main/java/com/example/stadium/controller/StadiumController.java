package com.example.stadium.controller;

import com.example.stadium.dto.AddStadiumDTO;
import com.example.stadium.model.Stadium;
import com.example.stadium.service.StadiumService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/stadium")
@RequiredArgsConstructor
public class StadiumController {

    private final StadiumService service;

    @PostMapping("/create")
    public ResponseEntity<Stadium>create(@RequestBody AddStadiumDTO addStadiumDTO){
        return ResponseEntity.status(HttpStatus.OK).body(service.create(addStadiumDTO));
    }

    @GetMapping("/{id}")
    public Stadium findStadiumById(@PathVariable("id")Long id){
        return service.findStadiumById(id);
    }


}
