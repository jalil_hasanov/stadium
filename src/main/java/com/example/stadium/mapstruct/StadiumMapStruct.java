package com.example.stadium.mapstruct;

import com.example.stadium.dto.AddStadiumDTO;
import com.example.stadium.model.Stadium;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StadiumMapStruct {

    AddStadiumDTO map(Stadium stadium);
    Stadium map(AddStadiumDTO stadium);
}
