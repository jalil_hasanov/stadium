package com.example.stadium.repository;

import com.example.stadium.dto.AddStadiumDTO;
import com.example.stadium.model.Stadium;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StadiumRepository extends JpaRepository<Stadium, Long> {
    Optional<Stadium> findStadiumById(Long id);
}
