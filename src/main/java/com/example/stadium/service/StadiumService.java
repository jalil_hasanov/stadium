package com.example.stadium.service;

import com.example.stadium.dto.AddStadiumDTO;
import com.example.stadium.model.Stadium;
import org.springframework.stereotype.Service;

@Service
public interface StadiumService {
    Stadium create(AddStadiumDTO addStadiumDTO);

    Stadium findStadiumById(Long id);
}
