package com.example.stadium.service.impl;

import com.example.stadium.dto.AddStadiumDTO;
import com.example.stadium.mapstruct.StadiumMapStruct;
import com.example.stadium.model.Stadium;
import com.example.stadium.repository.StadiumRepository;
import com.example.stadium.service.StadiumService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StadiumServiceImpl implements StadiumService {

    private final StadiumRepository repository;
    private final StadiumMapStruct mapStruct;
    @Override
    public Stadium create(AddStadiumDTO addStadiumDTO) {
        return repository.save(mapStruct.map(addStadiumDTO));
    }

    @Override
    public Stadium findStadiumById(Long id) {
        return repository.findStadiumById(id).orElse(new Stadium());
    }
}
